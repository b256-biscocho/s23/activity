/*
1. Create a trainer object using object literals.
Initialize/add the following trainer object properties:
Name (String)
Age (Number)
Pokemon (Array)
Friends (Object with Array values for properties)*/
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Snorlax", "Pikachu", "Squirtle", "Bulbasaur"],
	Friends : {hoenn:["May","Max"], kanto: ["Brock","Misty"]},
	///*Initialize/add the trainer object method named talk that prints out the message: Pikachu! I choose you!
	talk: function() {
		console.log("Pikachu! I choose you!")
	}
};
console.log(trainer);



// Access the trainer object properties using dot and square bracket notation.
// Using dot notation
console.log("Result from dot notation: ");
console.log(trainer.name);

//using square bracket notation
console.log("Result from square bracket notation: ");
console.log(trainer["pokemon"]);

//Invoke/call the trainer talk object method.
console.log("Result of talk method:")
trainer.talk()

/*

Create a constructor for creating a pokemon with the following properties:
Name (Provided as an argument to the constructor)
Level (Provided as an argument to the constructor)
Health (Create an equation that uses the level property)
Attack (Create an equation that uses the level property) */

function Pokemon(name, level, health, attack) {
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
};

//Create/instantiate several pokemon objects from the constructor with varying name and level properties.
let pikaStat = new Pokemon("Pikachu", 12);
console.log(pikaStat);

let geoStat = new Pokemon("Geodude", 8);
console.log(geoStat);

let mewStat = new Pokemon("Mewtwo", 100);
console.log(mewStat)



//Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
	// Create a faint method that will print out a message: targetPokemon has fainted.
	// Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
	// Invoke the tackle method of one pokemon object to see if it works as intended.


//this.geo vs target. pika
geoStat.tackle = function(targetPokemon) {
	console.log(this.name + " tackled " + targetPokemon.name);
	targetPokemon.health = targetPokemon.health - this.attack;
    // targetPokemon.health -= this.attack;
    console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);
    
    // Create a faint method that will print out a message: targetPokemon has fainted.
	// Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.

    if (targetPokemon.health <= 0) {
     geoStat.faint(targetPokemon);
    }
    geoStat.faint = function(targetPokemon) {
    console.log(targetPokemon.name + " fainted");
    }
  };

geoStat.tackle(pikaStat);
console.log(pikaStat);


//this.mew vs target.geo
mewStat.tackle = function(targetPokemon) {
	console.log(this.name + " tackled " + targetPokemon.name);
	targetPokemon.health = targetPokemon.health - this.attack;
    // targetPokemon.health -= this.attack;
    console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);
    
    // Create a faint method that will print out a message: targetPokemon has fainted.
	// Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.


    if (targetPokemon.health <= 0) {
     geoStat.faint(targetPokemon);
    }
    mewStat.faint = function(targetPokemon) {
    console.log(targetPokemon.name + " fainted");
  };
}
mewStat.tackle(geoStat);
console.log(geoStat);



//LOVED THIS ACTIVITY!! fun :D